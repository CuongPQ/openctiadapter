/// <reference path="~/scripts/jquery.js" />
/// <reference path="~/scripts/ww.jquery.js" />
(function ($) {
    
    $(function () {
        CreateH3Links();

        // force hash to reload        
        if (window.location.hash)
            window.location = window.location.hash;

        var commentId = null;
        $("#" + serverVars.txtBodyId)
            .focus(function() {
                OnTextTyped();
                commentId = setInterval(OnTextTyped, 2000);
            })
            .blur(function() { clearInterval(commentId); });

        $(".commentedit").click(commentEdit);
        $(".remove-comment").click(DeleteComment);


    });

    function CreateH3Links() {
        var $h3 = $(".postcontent>h3");
        
        $h3.each(function() {
            var $h3item = $(this);
            var tag = $h3item.text().replace(/\s+/g, "");

            var $a = $("<a />")
                .attr({
                    name: tag,
                    href: "#" + tag
                })
                .addClass('linkicon')
                .addClass('link-hidden');
            $h3item.prepend($a);

            $h3item
                .hover(
                    function() {
                        $a.removeClass("link-hidden");
                    },
                    function() {
                        $a.addClass("link-hidden");
                    })
                .click(function() {
                    window.location = $a.prop("href");
                });


        });

    }

    function OnTextTyped(event) {
        var Ctl = $("#" + serverVars.txtBodyId);
        var Ctl2 = $("#lblCommentCharCount");
        if (Ctl.length > 0 && Ctl2.length > 0) {
            var Size = Ctl.val().length;
            Ctl2.text(Size.toString() + " of " + serverVars.commentMaxLength + " characters");
        }

        Proxy.FormatComment(Ctl.val(), function(result) {
            if (result)
                $("#divCommentPreview")
                    .html("<b>" + $("#" + serverVars.txtTitleId).val() + "</b><br/>" +
                        "by " + $("#" + serverVars.txtAuthorId).val() + "<br /><br />" +
                        result).show();
        });
    }

    function DeleteComment() {
        var $el = $(this);
        var id = $el.data("id") * 1;        
        Proxy.DeleteComment(id,
            function(result) {
                if (!result) {
                    showStatus("Comment not deleted. Id is invalid or you're not logged in.");
                    return;
                }                
                $el.parents(".comment").fadeOut("slow", function() { $(this).remove(); });
            },
            function(error) {
                showStatus("Comment not deleted: " + error.message);
            }
        );
    }

    function commentEdit(evt) {
        var jComment = $(this).parents(".comment-panel-right").find(".commentbody");
        if (jComment.length < 1)
            return;

        jComment.contentEditable(
        {
            editClass: "contenteditable",
            saveHandler: function(e) {
                // grab id from parent .comment element and strip cmt_ prefix
                var id = jComment.parents(".comment").get(0).id.replace("cmt_", "");

                // call service to update comment with numeric id and updated html
                Proxy.UpdateCommentText(+id, jComment.html());

                // return true to close editor  (false leaves open)
                return true;
            }
        });
    }
})(jQuery);