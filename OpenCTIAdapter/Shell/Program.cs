﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Threading;
using System.Runtime.InteropServices;
using System.Security.AccessControl;
using System.Security.Principal;
using System.Reflection;
using OpenCTIAdapter.Shell;
using OpenCTIAdapter.WebConnector;

namespace OpenCTIAdapter
{
    static class Program
    {
        // A system-wide mutex used to ensure at most one adapter process can be launched at any time
        static private Mutex adapterProcessMutex = null;

        static private bool adapterProcessExists()
        {
            string appGuid = ((GuidAttribute)Assembly.GetExecutingAssembly().GetCustomAttributes(typeof(GuidAttribute), false).GetValue(0)).Value.ToString();
            string mutexName = string.Format("Global\\{{{0}}}", appGuid);

            // Create an "Everyone Full-Control" MutexSecurity
            var allowEveryoneRule = new MutexAccessRule(new SecurityIdentifier(WellKnownSidType.WorldSid, null), MutexRights.FullControl, AccessControlType.Allow);
            var mutexSec = new MutexSecurity();
            mutexSec.AddAccessRule(allowEveryoneRule);

            bool isFirstProcess;
            Program.adapterProcessMutex = new Mutex(true, mutexName, out isFirstProcess, mutexSec);

            return !isFirstProcess;
        }
        /// <summary>
        /// The main entry point for the application.
        /// </summary>
        [STAThread]
        static void Main()
        {
            if (adapterProcessExists())
            {
                MessageBox.Show(Constant.MultipleAdapterProcessError, "Error", MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
                return;
            }

            SignalRConnetion signalRConn = SignalRConnetion.Instance;
            signalRConn.start();

            About about = new About();
            Application.EnableVisualStyles();
            Application.Run(about);

            // Release process mutex
            if (Program.adapterProcessMutex != null)
            {
                Program.adapterProcessMutex.ReleaseMutex();
            }
        }
    }
}
