﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using System.Windows.Forms;
using System.Xml.Linq;
using System.Diagnostics;
using OpenCTIAdapter.WebConnector;
using OpenCTIAdapter.Shell;

namespace OpenCTIAdapter
{
    /// <summary>
    /// @Author: CuongPQ.
    /// @Date: 20 Aug 2015.
    /// @Description: Define form About content the menu tray.
    /// </summary>
    public partial class About : Form
    {
        public About()
        {
            InitializeComponent();
            this.Hide();
        }        

        /// <summary>
        /// @Author: CuongPQ.
        /// @Date: 20 Aug 2015.
        /// @Description: Handle action click into exit item.
        /// end thread
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void toolStripMenuItemExit_Click(object sender, EventArgs e)
        {
            SignalRConnetion signalRConn = SignalRConnetion.Instance;
            signalRConn.IsExit = true;
            // close the form, which closes the application.
            signalRConn.endConnection();
            Thread.Sleep(1);
            this.Close();
        }

        private void toolStripMenuItemAbout_Click(object sender, EventArgs e)
        {
            this.Show();
            this.WindowState = FormWindowState.Normal;
        }

        private void SalesforceCTIForm_FormClosing(object sender, FormClosingEventArgs e)
        {
            SignalRConnetion signalRConn = SignalRConnetion.Instance;
            if (signalRConn.IsExit)
            {
                e.Cancel = false;
            }
            else
            {
                e.Cancel = true;
            }
            this.Hide();
        }

        private void startAdapterToolStripMenuItem_Click(object sender, EventArgs e)
        {
            System.Windows.Forms.ToolStripMenuItem item = (System.Windows.Forms.ToolStripMenuItem)sender;
            notifyIconSalesforceCTI.BalloonTipIcon = ToolTipIcon.Info;
            item.CheckOnClick = !item.CheckOnClick;
            item.Checked = item.CheckOnClick;
            if (item.CheckOnClick)
            {
                notifyIconSalesforceCTI.BalloonTipTitle = "Open CTI Adapter is started";
            }
            else
            {
                notifyIconSalesforceCTI.BalloonTipTitle = "Open CTI Adapter is stopped";
            }
            
            notifyIconSalesforceCTI.BalloonTipText = "You have just minimized the application." +
                                        Environment.NewLine +
                                        "Right-click on the icon for more options.";
            UpdateTrayMenu(String.Empty);
            notifyIconSalesforceCTI.ShowBalloonTip(2000);
            
        }

        private void settingToolStripMenuItem_Click(object sender, EventArgs e)
        {
            Setting settingForm = new Setting();
            settingForm.Show();
        }

        public void UpdateTrayMenu(string xmlMenu)
        {
            // re-create the menu from scratch
            this.contextMenuStripSalesforceCTI.Items.Clear();
            this.contextMenuStripSalesforceCTI.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
                                                            this.startAdapterToolStripMenuItem,
                                                            this.settingToolStripMenuItem,
                                                            this.toolStripMenuItemAbout,
                                                            this.toolStripMenuItemExit});

            if (xmlMenu == null || xmlMenu == string.Empty)
            {
                return;
            }

            IEnumerable<XElement> items = XElement.Parse(xmlMenu).Elements("ITEM");

            if (items.Count() > 0)
            {
                // first insert a seperator at the top of the adapter menu
                this.contextMenuStripSalesforceCTI.Items.Insert(0, new ToolStripSeparator());

                // build the adapter menu from the bottom up
                foreach (XElement item in items.Reverse())
                {
                    XAttribute itemId = item.Attribute("ID");
                    XAttribute itemLabel = item.Attribute("LABEL");
                    XAttribute itemChecked = item.Attribute("CHECKED");
                    XAttribute itemSeparator = item.Attribute("SEPARATOR");

                    if (itemId != null && itemLabel != null)
                    {
                        ToolStripMenuItem mi = new ToolStripMenuItem(itemLabel.Value,
                                                                    null,
                                                                    new EventHandler(toolStripMenuItemAdapterAction_Click),
                                                                    itemId.Value);
                        mi.Checked = (itemChecked != null) ? bool.Parse(itemChecked.Value) : false;
                        this.contextMenuStripSalesforceCTI.Items.Insert(0, mi);
                    }
                    else if (itemSeparator != null && bool.Parse(itemSeparator.Value))
                    {
                        // insert a seperator
                        this.contextMenuStripSalesforceCTI.Items.Insert(0, new ToolStripSeparator());
                    }
                }
            }

            this.contextMenuStripSalesforceCTI.Refresh();
        }

        private void toolStripMenuItemAdapterAction_Click(object sender, EventArgs e)
        {
            //AdapterLink.SendCommand(((ToolStripMenuItem)sender).Name, null);
        }
    }
}
