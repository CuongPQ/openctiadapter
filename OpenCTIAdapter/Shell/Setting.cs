﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Security;
using OpenCTIAdapter.Utils;
using System.Text.RegularExpressions;

namespace OpenCTIAdapter.Shell
{
    public partial class Setting : Form
    {
        public Setting()
        {
            InitializeComponent();
            this.MaximumSize = this.MinimumSize = this.Size;
            this.CenterToScreen();
            initData();
            this.btnCancel.Select();
        }

        /// <summary>
        /// @Author: CuongPQ.
        /// @Date: 20 Aug 2015.
        /// Read data from cach file into setting form.
        /// </summary>
        private void initData()
        {
            Dictionary<string, string> value = new Dictionary<string, string>();
            value = FileUtils.readFile();
            if (value.Count > 0)
            {
                this.adapterPortValue.Text = value[Constant.SIGNALR_PORT];
                if (this.adapterPortValue.Text.Equals(String.Empty))
                {
                    this.adapterPortValue.Text = Constant.SIGNALR_PORT_DEFAULT.ToString();
                }
                this.pbxServerAddressValue.Text = value[Constant.SERVER];
                this.pbxPortValue.Text = value[Constant.PORT];
                this.pbxUsernameValue.Text = value[Constant.USERNAME];
                this.pbxSecretValue.Text = value[Constant.PASSWORD];
                this.pbxExtensionValue.Text = value[Constant.CHANNEL];
                this.pbxCallerNameValue.Text = value[Constant.CALLER_NAME];
            }

        }

        /// <summary>
        /// Validate only digit enter value for port.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void ValidatePortNumber_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (!char.IsControl(e.KeyChar) && !char.IsDigit(e.KeyChar) &&
                (e.KeyChar != '.'))
            {
                e.Handled = true;
            }

            // only allow one decimal point
            if ((e.KeyChar == '.') && ((sender as TextBox).Text.IndexOf('.') > -1))
            {
                e.Handled = true;
            }
        }

        private void btnCancel_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void btnOk_Click(object sender, EventArgs e)
        {
            if (checkCompleteFill())
            {
                Dictionary<string, string> value = new Dictionary<string, string>();
                value.Add(Constant.SIGNALR_PORT, Constant.SIGNALR_PORT + ":" + this.adapterPortValue.Text);
                value.Add(Constant.SERVER, Constant.SERVER + ":" + this.pbxServerAddressValue.Text.Replace(" ", String.Empty));
                value.Add(Constant.PORT, Constant.PORT + ":" + this.pbxPortValue.Text);
                value.Add(Constant.USERNAME, Constant.USERNAME + ":" + this.pbxUsernameValue.Text);
                value.Add(Constant.PASSWORD, Constant.PASSWORD + ":" + this.pbxSecretValue.Text);
                value.Add(Constant.CHANNEL, Constant.CHANNEL + ":" + this.pbxExtensionValue.Text);
                value.Add(Constant.CALLER_NAME, Constant.CALLER_NAME + ":" + this.pbxCallerNameValue.Text);
                FileUtils.writeFile(value);
                this.Close();
            }
            
        }

        /// <summary>
        /// check all required field is fill.
        /// </summary>
        private bool checkCompleteFill()
        {
            string pattern = @"^(?=\d+\.\d+\.\d+\.\d+$)(?:(?:25[0-5]|2[0-4][0-9]|1[0-9]{2}|[1-9][0-9]|[0-9])\.?){4}$";
            string adapterPort = SecurityElement.Escape(adapterPortValue.Text).Replace(" ", string.Empty);
            string serverAdd = SecurityElement.Escape(pbxServerAddressValue.Text);
            string port = SecurityElement.Escape(pbxPortValue.Text).Replace(" ", string.Empty);
            string username = SecurityElement.Escape(pbxUsernameValue.Text).Replace(" ", string.Empty);
            string password = SecurityElement.Escape(this.pbxSecretValue.Text).Replace(" ", string.Empty);
            string channel = SecurityElement.Escape(pbxExtensionValue.Text).Replace(" ", string.Empty);
            if (port.Length == 0 || username.Length == 0 || password.Length == 0 || channel.Length == 0)
            {
                MessageBox.Show("Please fill these fields required (*).", "Notify", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                return false;
            }
            else if (adapterPort.Length == 0)
            {
                MessageBox.Show("Adapter Port is empty, so we will set with default is 8089." + Constant.SIGNALR_PORT_DEFAULT, "Notify", MessageBoxButtons.OK, MessageBoxIcon.Warning);
            }
            else if (Regex.IsMatch(serverAdd, pattern) == false)
            {
                MessageBox.Show("The server address is invalid. Please check again.", "Notify", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                return false;
            }
            return true;

        }
    }
}
