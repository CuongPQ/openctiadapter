﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace OpenCTIAdapter.Shell
{
    /// <summary>
    /// @Author: CuongPQ.
    /// @Date: 20 Aug 2015.
    /// @Description: Define the contant variable use for project.
    /// </summary>
    public static class  Constant
    {
        // labels
        public const string MultipleAdapterProcessError = "The CTI adapter cannot run. Please make sure there is no other adapter process running on this machine.";

        //Host Address of SignalR
        public const string SIGNALR_HOST = "https://localhost";

        //Default Port of SignalR
        public const int SIGNALR_PORT_DEFAULT = 8089;

        public const string SERVER_NAME = "TTV's SIGNALR Server";
        public static readonly string KEY_QUERY_SYSTEM_INFORMATION = "Win32_Processor";
        public static readonly string PROCESSOR_ID_NAME = "ProcessorId";
        public static readonly string DEFAULT_PASSWORD = "0505211090";
        
        //information for signalR configure.
        public static readonly string SIGNALR_PORT = "SignalR Port";
        //information for pbx configure.
        public static readonly string SERVER = "Server";
        public static readonly string PORT = "Port";
        public static readonly string USERNAME = "Username";
        public static readonly string PASSWORD = "Password";
        public static readonly string CHANNEL = "Channel";
        public static readonly string CALLER_NAME = "Caller";
    }
}
