﻿namespace OpenCTIAdapter
{
    partial class About
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(About));
            this.notifyIconSalesforceCTI = new System.Windows.Forms.NotifyIcon(this.components);
            this.contextMenuStripSalesforceCTI = new System.Windows.Forms.ContextMenuStrip(this.components);
            this.startAdapterToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.settingToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripMenuItemAbout = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripMenuItemExit = new System.Windows.Forms.ToolStripMenuItem();
            this.exitToolStripMenuItem2 = new System.Windows.Forms.ToolStripMenuItem();
            this.appName = new System.Windows.Forms.Label();
            this.callCenterEdition = new System.Windows.Forms.Label();
            this.contextMenuStripSalesforceCTI.SuspendLayout();
            this.SuspendLayout();
            // 
            // notifyIconSalesforceCTI
            // 
            this.notifyIconSalesforceCTI.BalloonTipText = "TTV Salesforce Call Center Adapter";
            this.notifyIconSalesforceCTI.ContextMenuStrip = this.contextMenuStripSalesforceCTI;
            this.notifyIconSalesforceCTI.Icon = ((System.Drawing.Icon)(resources.GetObject("notifyIconSalesforceCTI.Icon")));
            this.notifyIconSalesforceCTI.Text = "TTV Salesforce Call Center Adapter";
            this.notifyIconSalesforceCTI.Visible = true;
            // 
            // contextMenuStripSalesforceCTI
            // 
            this.contextMenuStripSalesforceCTI.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.startAdapterToolStripMenuItem,
            this.settingToolStripMenuItem,
            this.toolStripMenuItemAbout,
            this.toolStripMenuItemExit});
            this.contextMenuStripSalesforceCTI.Name = "contextMenuStripSalesforceCTI";
            this.contextMenuStripSalesforceCTI.Size = new System.Drawing.Size(144, 92);
            // 
            // startAdapterToolStripMenuItem
            // 
            this.startAdapterToolStripMenuItem.Name = "startAdapterToolStripMenuItem";
            this.startAdapterToolStripMenuItem.Size = new System.Drawing.Size(143, 22);
            this.startAdapterToolStripMenuItem.Text = "Start Adapter";
            this.startAdapterToolStripMenuItem.Click += new System.EventHandler(this.startAdapterToolStripMenuItem_Click);
            // 
            // settingToolStripMenuItem
            // 
            this.settingToolStripMenuItem.Name = "settingToolStripMenuItem";
            this.settingToolStripMenuItem.Size = new System.Drawing.Size(143, 22);
            this.settingToolStripMenuItem.Text = "Setting";
            this.settingToolStripMenuItem.Click += new System.EventHandler(this.settingToolStripMenuItem_Click);
            // 
            // toolStripMenuItemAbout
            // 
            this.toolStripMenuItemAbout.Name = "toolStripMenuItemAbout";
            this.toolStripMenuItemAbout.Size = new System.Drawing.Size(143, 22);
            this.toolStripMenuItemAbout.Text = "About";
            this.toolStripMenuItemAbout.Click += new System.EventHandler(this.toolStripMenuItemAbout_Click);
            // 
            // toolStripMenuItemExit
            // 
            this.toolStripMenuItemExit.Name = "toolStripMenuItemExit";
            this.toolStripMenuItemExit.Size = new System.Drawing.Size(143, 22);
            this.toolStripMenuItemExit.Text = "Exit";
            this.toolStripMenuItemExit.Click += new System.EventHandler(this.toolStripMenuItemExit_Click);
            // 
            // appName
            // 
            this.appName.AutoSize = true;
            this.appName.BackColor = System.Drawing.Color.Transparent;
            this.appName.Font = new System.Drawing.Font("Tahoma", 14.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Pixel, ((byte)(0)));
            this.appName.ForeColor = System.Drawing.Color.Red;
            this.appName.Location = new System.Drawing.Point(308, 103);
            this.appName.Margin = new System.Windows.Forms.Padding(0);
            this.appName.Name = "appName";
            this.appName.Size = new System.Drawing.Size(140, 18);
            this.appName.TabIndex = 1;
            this.appName.Text = "Open CTI Adapter";
            // 
            // callCenterEdition
            // 
            this.callCenterEdition.AutoSize = true;
            this.callCenterEdition.BackColor = System.Drawing.Color.Transparent;
            this.callCenterEdition.Font = new System.Drawing.Font("Tahoma", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Pixel, ((byte)(0)));
            this.callCenterEdition.Location = new System.Drawing.Point(343, 136);
            this.callCenterEdition.Margin = new System.Windows.Forms.Padding(0);
            this.callCenterEdition.Name = "callCenterEdition";
            this.callCenterEdition.Size = new System.Drawing.Size(64, 18);
            this.callCenterEdition.TabIndex = 30;
            this.callCenterEdition.Text = "Beta 1.0";
            // 
            // About
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackgroundImage = global::OpenCTIAdapter.Properties.Resources.ctiSplashScreen;
            this.ClientSize = new System.Drawing.Size(493, 242);
            this.Controls.Add(this.callCenterEdition);
            this.Controls.Add(this.appName);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedDialog;
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "About";
            this.ShowIcon = false;
            this.ShowInTaskbar = false;
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "About";
            this.WindowState = System.Windows.Forms.FormWindowState.Minimized;
            this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.SalesforceCTIForm_FormClosing);
            this.contextMenuStripSalesforceCTI.ResumeLayout(false);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion
        private System.Windows.Forms.NotifyIcon notifyIconSalesforceCTI;
        private System.Windows.Forms.ContextMenuStrip contextMenuStripSalesforceCTI;
        private System.Windows.Forms.ToolStripMenuItem toolStripMenuItemAbout;
        private System.Windows.Forms.ToolStripMenuItem toolStripMenuItemExit;
        private System.Windows.Forms.ToolStripMenuItem exitToolStripMenuItem2;
        private System.Windows.Forms.Label appName;
        private System.Windows.Forms.Label callCenterEdition;
        private System.Windows.Forms.ToolStripMenuItem settingToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem startAdapterToolStripMenuItem;
    }
}

