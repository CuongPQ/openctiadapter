﻿namespace OpenCTIAdapter.Shell
{
    partial class Setting
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(Setting));
            this.AdapterServerSetting = new System.Windows.Forms.Label();
            this.seesionLine = new System.Windows.Forms.Label();
            this.adapterPort = new System.Windows.Forms.Label();
            this.adapterPortValue = new System.Windows.Forms.TextBox();
            this.pbxSettingLine = new System.Windows.Forms.Label();
            this.pbxSetting = new System.Windows.Forms.Label();
            this.pbxServerAddress = new System.Windows.Forms.Label();
            this.pbxServerAddressValue = new iptb.IPTextBox();
            this.pbxPort = new System.Windows.Forms.Label();
            this.pbxPortValue = new System.Windows.Forms.TextBox();
            this.pbxExtension = new System.Windows.Forms.Label();
            this.pbxExtensionValue = new System.Windows.Forms.TextBox();
            this.pbxCallerName = new System.Windows.Forms.Label();
            this.pbxCallerNameValue = new System.Windows.Forms.TextBox();
            this.btnOk = new System.Windows.Forms.Button();
            this.btnCancel = new System.Windows.Forms.Button();
            this.pbxSecret = new System.Windows.Forms.Label();
            this.pbxSecretValue = new System.Windows.Forms.TextBox();
            this.pbxUsername = new System.Windows.Forms.Label();
            this.pbxUsernameValue = new System.Windows.Forms.TextBox();
            this.label1 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.label5 = new System.Windows.Forms.Label();
            this.SuspendLayout();
            // 
            // AdapterServerSetting
            // 
            this.AdapterServerSetting.AutoSize = true;
            this.AdapterServerSetting.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.AdapterServerSetting.Location = new System.Drawing.Point(0, 9);
            this.AdapterServerSetting.Name = "AdapterServerSetting";
            this.AdapterServerSetting.Size = new System.Drawing.Size(136, 13);
            this.AdapterServerSetting.TabIndex = 0;
            this.AdapterServerSetting.Text = "Adapter Server Setting";
            // 
            // seesionLine
            // 
            this.seesionLine.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.seesionLine.Font = new System.Drawing.Font("Microsoft Sans Serif", 2F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.seesionLine.Location = new System.Drawing.Point(0, 25);
            this.seesionLine.Name = "seesionLine";
            this.seesionLine.Size = new System.Drawing.Size(285, 2);
            this.seesionLine.TabIndex = 1;
            // 
            // adapterPort
            // 
            this.adapterPort.AutoSize = true;
            this.adapterPort.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.adapterPort.Location = new System.Drawing.Point(17, 33);
            this.adapterPort.Name = "adapterPort";
            this.adapterPort.Size = new System.Drawing.Size(66, 13);
            this.adapterPort.TabIndex = 2;
            this.adapterPort.Text = "Adapter Port";
            // 
            // adapterPortValue
            // 
            this.adapterPortValue.Location = new System.Drawing.Point(125, 30);
            this.adapterPortValue.MaxLength = 5;
            this.adapterPortValue.Name = "adapterPortValue";
            this.adapterPortValue.Size = new System.Drawing.Size(127, 20);
            this.adapterPortValue.TabIndex = 3;
            this.adapterPortValue.Text = "8089";
            this.adapterPortValue.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.ValidatePortNumber_KeyPress);
            // 
            // pbxSettingLine
            // 
            this.pbxSettingLine.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.pbxSettingLine.Font = new System.Drawing.Font("Microsoft Sans Serif", 2F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.pbxSettingLine.Location = new System.Drawing.Point(0, 78);
            this.pbxSettingLine.Name = "pbxSettingLine";
            this.pbxSettingLine.Size = new System.Drawing.Size(285, 2);
            this.pbxSettingLine.TabIndex = 4;
            // 
            // pbxSetting
            // 
            this.pbxSetting.AutoSize = true;
            this.pbxSetting.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.pbxSetting.Location = new System.Drawing.Point(0, 60);
            this.pbxSetting.Name = "pbxSetting";
            this.pbxSetting.Size = new System.Drawing.Size(132, 13);
            this.pbxSetting.TabIndex = 5;
            this.pbxSetting.Text = "PBX Setting (Asterisk)";
            // 
            // pbxServerAddress
            // 
            this.pbxServerAddress.AutoSize = true;
            this.pbxServerAddress.Location = new System.Drawing.Point(17, 88);
            this.pbxServerAddress.Name = "pbxServerAddress";
            this.pbxServerAddress.Size = new System.Drawing.Size(79, 13);
            this.pbxServerAddress.TabIndex = 6;
            this.pbxServerAddress.Text = "Server Address";
            // 
            // pbxServerAddressValue
            // 
            this.pbxServerAddressValue.Location = new System.Drawing.Point(125, 83);
            this.pbxServerAddressValue.Name = "pbxServerAddressValue";
            this.pbxServerAddressValue.Size = new System.Drawing.Size(131, 20);
            this.pbxServerAddressValue.TabIndex = 4;
            this.pbxServerAddressValue.ToolTipText = "";
            // 
            // pbxPort
            // 
            this.pbxPort.AutoSize = true;
            this.pbxPort.Location = new System.Drawing.Point(17, 114);
            this.pbxPort.Name = "pbxPort";
            this.pbxPort.Size = new System.Drawing.Size(26, 13);
            this.pbxPort.TabIndex = 8;
            this.pbxPort.Text = "Port";
            // 
            // pbxPortValue
            // 
            this.pbxPortValue.Location = new System.Drawing.Point(125, 111);
            this.pbxPortValue.Name = "pbxPortValue";
            this.pbxPortValue.Size = new System.Drawing.Size(129, 20);
            this.pbxPortValue.TabIndex = 5;
            this.pbxPortValue.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.ValidatePortNumber_KeyPress);
            // 
            // pbxExtension
            // 
            this.pbxExtension.AutoSize = true;
            this.pbxExtension.Location = new System.Drawing.Point(17, 138);
            this.pbxExtension.Name = "pbxExtension";
            this.pbxExtension.Size = new System.Drawing.Size(53, 13);
            this.pbxExtension.TabIndex = 10;
            this.pbxExtension.Text = "Extension";
            // 
            // pbxExtensionValue
            // 
            this.pbxExtensionValue.Location = new System.Drawing.Point(125, 135);
            this.pbxExtensionValue.Name = "pbxExtensionValue";
            this.pbxExtensionValue.Size = new System.Drawing.Size(129, 20);
            this.pbxExtensionValue.TabIndex = 6;
            // 
            // pbxCallerName
            // 
            this.pbxCallerName.AutoSize = true;
            this.pbxCallerName.Location = new System.Drawing.Point(17, 208);
            this.pbxCallerName.Name = "pbxCallerName";
            this.pbxCallerName.Size = new System.Drawing.Size(64, 13);
            this.pbxCallerName.TabIndex = 12;
            this.pbxCallerName.Text = "Caller Name";
            // 
            // pbxCallerNameValue
            // 
            this.pbxCallerNameValue.Location = new System.Drawing.Point(125, 205);
            this.pbxCallerNameValue.Name = "pbxCallerNameValue";
            this.pbxCallerNameValue.Size = new System.Drawing.Size(129, 20);
            this.pbxCallerNameValue.TabIndex = 9;
            // 
            // btnOk
            // 
            this.btnOk.Location = new System.Drawing.Point(57, 238);
            this.btnOk.Name = "btnOk";
            this.btnOk.Size = new System.Drawing.Size(75, 23);
            this.btnOk.TabIndex = 14;
            this.btnOk.Text = "Ok";
            this.btnOk.UseVisualStyleBackColor = true;
            this.btnOk.Click += new System.EventHandler(this.btnOk_Click);
            // 
            // btnCancel
            // 
            this.btnCancel.Location = new System.Drawing.Point(163, 238);
            this.btnCancel.Name = "btnCancel";
            this.btnCancel.Size = new System.Drawing.Size(75, 23);
            this.btnCancel.TabIndex = 15;
            this.btnCancel.Text = "Cancel";
            this.btnCancel.UseVisualStyleBackColor = true;
            this.btnCancel.Click += new System.EventHandler(this.btnCancel_Click);
            // 
            // pbxSecret
            // 
            this.pbxSecret.AutoSize = true;
            this.pbxSecret.Location = new System.Drawing.Point(17, 185);
            this.pbxSecret.Name = "pbxSecret";
            this.pbxSecret.Size = new System.Drawing.Size(38, 13);
            this.pbxSecret.TabIndex = 16;
            this.pbxSecret.Text = "Secret";
            // 
            // pbxSecretValue
            // 
            this.pbxSecretValue.Location = new System.Drawing.Point(125, 182);
            this.pbxSecretValue.Name = "pbxSecretValue";
            this.pbxSecretValue.PasswordChar = '*';
            this.pbxSecretValue.Size = new System.Drawing.Size(129, 20);
            this.pbxSecretValue.TabIndex = 8;
            // 
            // pbxUsername
            // 
            this.pbxUsername.AutoSize = true;
            this.pbxUsername.Location = new System.Drawing.Point(17, 162);
            this.pbxUsername.Name = "pbxUsername";
            this.pbxUsername.Size = new System.Drawing.Size(55, 13);
            this.pbxUsername.TabIndex = 18;
            this.pbxUsername.Text = "Username";
            // 
            // pbxUsernameValue
            // 
            this.pbxUsernameValue.Location = new System.Drawing.Point(125, 159);
            this.pbxUsernameValue.Name = "pbxUsernameValue";
            this.pbxUsernameValue.Size = new System.Drawing.Size(129, 20);
            this.pbxUsernameValue.TabIndex = 7;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.ForeColor = System.Drawing.Color.Red;
            this.label1.Location = new System.Drawing.Point(100, 88);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(11, 13);
            this.label1.TabIndex = 20;
            this.label1.Text = "*";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.ForeColor = System.Drawing.Color.Red;
            this.label2.Location = new System.Drawing.Point(44, 114);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(11, 13);
            this.label2.TabIndex = 21;
            this.label2.Text = "*";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.ForeColor = System.Drawing.Color.Red;
            this.label3.Location = new System.Drawing.Point(70, 138);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(11, 13);
            this.label3.TabIndex = 22;
            this.label3.Text = "*";
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.ForeColor = System.Drawing.Color.Red;
            this.label4.Location = new System.Drawing.Point(72, 162);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(11, 13);
            this.label4.TabIndex = 23;
            this.label4.Text = "*";
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.ForeColor = System.Drawing.Color.Red;
            this.label5.Location = new System.Drawing.Point(59, 185);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(11, 13);
            this.label5.TabIndex = 24;
            this.label5.Text = "*";
            // 
            // Setting
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(284, 274);
            this.Controls.Add(this.label5);
            this.Controls.Add(this.label4);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.pbxUsernameValue);
            this.Controls.Add(this.pbxUsername);
            this.Controls.Add(this.pbxSecretValue);
            this.Controls.Add(this.pbxSecret);
            this.Controls.Add(this.btnCancel);
            this.Controls.Add(this.btnOk);
            this.Controls.Add(this.pbxCallerNameValue);
            this.Controls.Add(this.pbxCallerName);
            this.Controls.Add(this.pbxExtensionValue);
            this.Controls.Add(this.pbxExtension);
            this.Controls.Add(this.pbxPortValue);
            this.Controls.Add(this.pbxPort);
            this.Controls.Add(this.pbxServerAddressValue);
            this.Controls.Add(this.pbxServerAddress);
            this.Controls.Add(this.pbxSetting);
            this.Controls.Add(this.pbxSettingLine);
            this.Controls.Add(this.adapterPortValue);
            this.Controls.Add(this.adapterPort);
            this.Controls.Add(this.seesionLine);
            this.Controls.Add(this.AdapterServerSetting);
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.MaximizeBox = false;
            this.Name = "Setting";
            this.Text = "Settings";
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label AdapterServerSetting;
        private System.Windows.Forms.Label seesionLine;
        private System.Windows.Forms.Label adapterPort;
        private System.Windows.Forms.TextBox adapterPortValue;
        private System.Windows.Forms.Label pbxSettingLine;
        private System.Windows.Forms.Label pbxSetting;
        private System.Windows.Forms.Label pbxServerAddress;
        private iptb.IPTextBox pbxServerAddressValue;
        private System.Windows.Forms.Label pbxPort;
        private System.Windows.Forms.TextBox pbxPortValue;
        private System.Windows.Forms.Label pbxExtension;
        private System.Windows.Forms.TextBox pbxExtensionValue;
        private System.Windows.Forms.Label pbxCallerName;
        private System.Windows.Forms.TextBox pbxCallerNameValue;
        private System.Windows.Forms.Button btnOk;
        private System.Windows.Forms.Button btnCancel;
        private System.Windows.Forms.Label pbxSecret;
        private System.Windows.Forms.TextBox pbxSecretValue;
        private System.Windows.Forms.Label pbxUsername;
        private System.Windows.Forms.TextBox pbxUsernameValue;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label label5;
    }
}