﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Threading;
using OpenCTIAdapter.Shell;
using Microsoft.AspNet.SignalR;
using Microsoft.Owin;
using Microsoft.Owin.Hosting;
using System.Diagnostics;

namespace OpenCTIAdapter.WebConnector
{
    class SignalRConnetion
    {
        private static volatile SignalRConnetion instance;
        private static object syncRoot = new Object();
        private static string SIGNALR_THREAD_NAME = "SignalR Connection Thread";
        private List<Thread> listThread { get; set; }
        private bool isExit;
        private int hostPort { get; set; }

        public bool IsExit
        {
            get { return isExit; }
            set { isExit = value; }
        }

        /// <summary>
        /// @Author: CuongPQ.
        /// @Date: 20 Aug 2015.
        /// @Desciption: Define the singleton object use throughtout the project.
        /// </summary>
        public static SignalRConnetion Instance
        {
            get
            {
                if (instance == null)
                {
                    lock (syncRoot)
                    {
                        if (instance == null)
                            instance = new SignalRConnetion();
                    }
                }

                return instance;
            }
        }

        public SignalRConnetion()
        {
            this.listThread = new List<Thread>();
        }

        /**
        * @Author: CuongPQ.
        * @Date: 20 Aug 2015.
        * create new thread to start socket connetion SignalR.
        * 
        */
        public void start()
        {
            reset();
            Thread signalRThread = new Thread(startConnection);
            signalRThread.Name = SIGNALR_THREAD_NAME;
            this.listThread.Add(signalRThread);
            signalRThread.Start();
        }

        public void reset()
        {
            this.isExit = false;
        }

        /**
         * @Author: CuongPQ.
         * @Date: 20 Aug 2015.
         * start connection to signalR server to make communication between Server and client side on Salesforce.
         * when the application end => end the connection thread.
         */
        public void startConnection()
        {
            if (this.hostPort == null || this.hostPort <= 0)
            {
                this.hostPort = Constant.SIGNALR_PORT_DEFAULT;
            }
            using (WebApp.Start<Startup>(Constant.SIGNALR_HOST + ":" + this.hostPort))
            {
                while (true)
                {
                    if( this.isExit == true)
                    {
                        break;
                    }
                }
            }
        }

        /**
         * @Author: CuongPQ.
         * @Date: 20 Aug 2015.
         * handle abort all thread of signalR.
         */
        public void endConnection()
        {
            foreach (Thread proc in this.listThread)
            {
                proc.Abort();
            }
            this.listThread.Clear();
        }

        /**
         * @Author: CuongPQ.
         * @Date: 20 Aug 2015.
         * send the message from server to client.
         * addMessagefromServer(Constant.SERVER_NAME, mess) is define on client page.
         * 
         */
        public void sendMessFromServer ( string mess)
        {
            IHubContext hubContext = GlobalHost.ConnectionManager.GetHubContext<ConnectionHub>();
            hubContext.Clients.All.addMessagefromServer(Constant.SERVER_NAME, mess);
        }
    }
}
