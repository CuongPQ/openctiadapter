﻿using Microsoft.AspNet.SignalR;
using Microsoft.Owin.Cors;
using Owin;

namespace OpenCTIAdapter.WebConnector
{
    /// <summary>
    /// @Author: CuongPQ.
    /// @Date: 20 Aug 2015.
    /// @Description: make the startup configuration for signal connetion.
    /// </summary>
    class Startup
    {
        public void Configuration(IAppBuilder app)
        {
            //add more debug logging into it.
            GlobalHost.HubPipeline.AddModule(new LoggingPipelineModule()); 
            // Branch the pipeline here for requests that start with "/signalr"
            app.Map("/connection", map =>
            {
                // Setup the CORS middleware to run before SignalR.
                // By default this will allow all origins. You can 
                // configure the set of origins and/or http verbs by
                // providing a cors options with a different policy.
                map.UseCors(CorsOptions.AllowAll);
                var hubConfiguration = new HubConfiguration
                {
                    // You can enable JSONP by uncommenting line below.
                    // JSONP requests are insecure but some older browsers (and some
                    // versions of IE) require JSONP to work cross domain
                    // EnableJSONP = true
                };
                // Run the SignalR pipeline. We're not using MapSignalR
                // since this branch already runs under the "/signalr"
                // path.

                hubConfiguration.EnableDetailedErrors = true;
                map.RunSignalR(hubConfiguration);
            });
        }
    }
}
