﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Management;
using OpenCTIAdapter.Shell;

namespace OpenCTIAdapter.Utils
{
    /// <summary>
    /// @Author: CuongPQ.
    /// @Date : 20 Aug 2015.
    /// @Description: Define the util method use for this project.
    /// </summary>
    class Util
    {   
        /**
         * convert string value into integer value.
         */
        public static int convertInt(string input)
        {
            int numVal = -1;
            try
            {
                numVal = Convert.ToInt32(input);
            }
            catch (FormatException e)
            {
                Console.WriteLine("Input string is not a sequence of digits.");
            }
            catch (OverflowException e)
            {
                Console.WriteLine("The number cannot fit in an Int32.");
            }
            finally
            {
                if (numVal < Int32.MaxValue)
                {
                    Console.WriteLine("The new value is {0}", numVal + 1);
                }
                else
                {
                    Console.WriteLine("numVal cannot be incremented beyond its current value");
                }
            }
            return numVal;
        }

        /**
         * get last extention in string value.
         */
        public static string getLastExtention(string value, string pRegex)
        {
            string[] val = Regex.Split(value, pRegex);
            if (val.Length > 1)
            {
                return val[val.Length - 1];
            }
            return string.Empty;
        }

        /**
         * get current date time and replace space into empty string.
         */
        public static string getCurrentDateTime()
        {
            DateTime now = DateTime.Now;
            string dateTime = (now.ToString()).Replace(" ", string.Empty);
            dateTime = dateTime.Replace("/", "-");
            dateTime = dateTime.Replace(":", "-");
            return dateTime;
        }

        /**
         * find the processor id.
         */
        public static string generatePassord()
        {
            /*
            ManagementObjectSearcher searcher = new ManagementObjectSearcher("select * from " + Constant.KEY_QUERY_SYSTEM_INFORMATION);
            try
            {
                foreach (ManagementObject share in searcher.Get())
                {
                    foreach (PropertyData PC in share.Properties)
                    {
                        if (PC.Name.Equals(Constant.PROCESSOR_ID_NAME))
                        {
                            return PC.Value.ToString();
                        }
                    }
                }
            }
            catch (Exception exp)
            {

            }
            */
            try
            {
                var mbs = new ManagementObjectSearcher("Select ProcessorId From Win32_processor");
                ManagementObjectCollection mbsList = mbs.Get();
                string id = "";
                foreach (ManagementObject mo in mbsList)
                {
                    id = mo["ProcessorId"].ToString();
                    break;
                }
                return id;
            }
            catch(Exception e)
            {

            }

            return Constant.DEFAULT_PASSWORD;
        }

    }
}
