﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.IO;
using System.Security;
using System.Windows.Forms;
using OpenCTIAdapter.Shell;
using OpenCTIAdapter.Utils;

namespace OpenCTIAdapter.Utils
{
    /// <summary>
    /// @Author: CUongpq.
    /// @Date: 20 Aug 2015.
    /// @Description: Read/ write file configuration setting.
    /// </summary>
    class FileUtils
    {
       public static void writeFile(Dictionary<string, string> value)
        {
            try
            {
                string browserConnectorLogFileName = //(Settings.Default.BrowserConnectorLogFile != string.Empty) ?
                    //Settings.Default.BrowserConnectorLogFile :
                                               Directory.GetCurrentDirectory() + "\\config.txt";
                //Pass the filepath and filename to the StreamWriter Constructor
                StreamWriter sw = new StreamWriter(browserConnectorLogFileName);
                foreach (KeyValuePair<string, string> keyValuePair in value)
                {
                    RC4 rc4 = new RC4(Util.generatePassord(), SecurityElement.Escape(keyValuePair.Value));
                    string temp = RC4.StrToHexStr(rc4.EnDeCrypt());
                    sw.WriteLine(temp);
                }
                //Close the file
                sw.Close();
            }
            catch (Exception e)
            {
                MessageBox.Show("Exception_write_file: " + e.Message);
            }
            finally
            {
                //MessageBox.Show("Executing finally block.");
            }
        }

        public static Dictionary<string, string> readFile()
        {
            Dictionary<string, string> value = new Dictionary<string, string>();
            string line;
            try
            {
                string browserConnectorLogFileName = //(Settings.Default.BrowserConnectorLogFile != string.Empty) ?
                    //Settings.Default.BrowserConnectorLogFile :
                                               Directory.GetCurrentDirectory() + "\\config.txt";
                //Pass the file path and file name to the StreamReader constructor
                StreamReader sr = new StreamReader(browserConnectorLogFileName);

                //Read the first line of text
                line = sr.ReadLine();


                //Continue to read until you reach end of file
                while (line != null)
                {
                    RC4 rc4 = new RC4(Util.generatePassord(), line);
                    rc4.Text = RC4.HexStrToStr(line);
                    line = rc4.EnDeCrypt();
                    string[] val = line.Split(':');
                    if (val.Length > 1)
                    {
                        if (line.Contains(Constant.SIGNALR_PORT))
                        {
                            value.Add(Constant.SIGNALR_PORT, val[1]);
                        }
                        else if (line.Contains(Constant.SERVER))
                        {
                            value.Add(Constant.SERVER, val[1]);
                        }
                        else if (line.Contains(Constant.PORT))
                        {
                            value.Add(Constant.PORT, val[1]);
                        }
                        else if (line.Contains(Constant.USERNAME))
                        {
                            value.Add(Constant.USERNAME, val[1]);
                        }
                        else if (line.Contains(Constant.PASSWORD))
                        {
                            value.Add(Constant.PASSWORD, val[1]);
                        }
                        else if (line.Contains(Constant.CHANNEL))
                        {
                            value.Add(Constant.CHANNEL, val[1]);
                        }
                        else if (line.Contains(Constant.CALLER_NAME))
                        {
                            value.Add(Constant.CALLER_NAME, val[1]);
                        }
                    }

                    //Read the next line
                    line = sr.ReadLine();
                }

                //close the file
                sr.Close();
                Console.ReadLine();
            }
            catch (Exception e)
            {
                Console.WriteLine("Exception: " + e.Message);
            }
            finally
            {
                Console.WriteLine("Executing finally block.");
            }
            return value;
        }
    }
}
